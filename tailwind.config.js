/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./public/**/*.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  theme: {
    colors: {
      "main-green": "#03AC0E",
      "green-600": "#049C0E",
      "green-200": "rgba(3, 172, 14, 0.22)",
      "main-blue": "#1577B4",
      "red-500": "#ED0A2A",
      "black-500": "#7A7A7A",
      "black-800": "#292929",
      "black-900": "#333333",
      white: "#fff",
      "white-100": "rgba(255, 255, 255, 0.8)",
      "white-200": "#FAFAFA",
      "white-400": "#F5F5F5",
      "white-500": "#EBEBEB",
      "white-800": "#C8C8C8",
    },
    extend: {},
  },
  plugins: [],
};

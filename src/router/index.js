import { createWebHistory, createRouter } from "vue-router";

import Login from "../view/Login.vue";
import Register from "../view/Register.vue";
import VerifikasiOTP from "../view/VerifikasiOTP.vue";
import Category from "../view/Category.vue";
import Product from "../view/Product.vue";
import DetailProduct from "../view/DetailProduct.vue";
import Cart from "../view/Cart.vue";
import Home from "../view/Home.vue";

const routes = [
  {
    path: "/",
    component: Login,
  },
  {
    path: "/login",
    component: Login,
  },
  {
    path: "/register",
    component: Register,
  },
  {
    path: "/verifikasi-otp",
    component: VerifikasiOTP,
  },
  {
    path: "/home",
    component: Home,
  },
  {
    path: "/category",
    component: Category,
  },
  {
    path: "/category/:category",
    component: Product,
  },
  {
    path: "/category/:category/:id",
    component: DetailProduct,
  },
  {
    path: "/cart",
    component: Cart,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
